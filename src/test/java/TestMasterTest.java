import com.genius.Genius.Util.Class.ClassUtils;
import com.genius.Genius.Util.TestMaster.TestMasterLog;
import com.genius.Genius.Util.TestMaster.TestMasterRule;
import com.genius.Genius.Util.TestMaster.TestObject;
import com.genius.Genius.Util.Unsafe.JavaUnSafe;
import org.junit.Rule;
import org.junit.Test;
import org.openjdk.jol.info.ClassLayout;
import sun.misc.Unsafe;

import java.io.IOException;
import java.io.Serializable;

public class TestMasterTest extends TestObject{

    @Rule
    public TestMasterRule testMasterRule2 = new TestMasterRule((t)->{
       TestMasterLog.logMemory(t);
    });
//
    @Rule
    public TestMasterRule testMasterRule = new TestMasterRule((t)->{
        TestMasterLog.logThread(t);
    });


//    @Test
//    public void Hello(){
//        System.out.println("this is test2");
//    }


    @Test
    public void Test(){
       Object o = null;
        System.out.println(ClassLayout.parseInstance(o).toPrintable());
    }

    private void Unsafe() {
        int[] arr = {1,2,3,4,5};
        Unsafe unsafe = JavaUnSafe.getUnsafe();

        int start_address = unsafe.arrayBaseOffset(arr.getClass());
        int index_scale = unsafe.arrayIndexScale(arr.getClass());
        System.out.println("起始位置:"+start_address+" 偏移量:"+index_scale);
        System.out.println(unsafe.getInt(arr,start_address+index_scale));
        unsafe.putInt(arr,start_address+index_scale,12);
        for (int i = 0; i < arr.length ; i++) {
            System.out.println(arr[i]);
        }
    }

    private static void test(TestPerson person){
        System.out.println(person);
    }

    @Test
    public void Test2(){
        StackTraceElement[] stack = new Throwable().getStackTrace();
        System.out.println(stack[stack.length - 1].getClassName());
    }


    static class TestPerson implements Serializable {
        String name;
        int age;
        boolean sex;
        int Id;
        String date;

        public TestPerson(String name, int age, boolean sex, int id, String date) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            Id = id;
            this.date = date;
        }

        @Override
        public String toString() {
            return "TestPerson{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    ", sex=" + sex +
                    ", Id=" + Id +
                    ", date='" + date + '\'' +
                    '}';
        }

        public static void main(String[] args) {
            StackTraceElement[] stack = new Throwable().getStackTrace();
            System.out.println(stack[stack.length - 1].getClassName());
        }
    }
}
