import com.genius.Genius.Polymer.Tree.BPtree.BPlusTree;
import com.genius.Genius.Util.TestMaster.TestMasterLog;
import com.genius.Genius.Util.TestMaster.TestObject;
import org.junit.Test;

import java.util.*;

public class TestBinarySearch extends TestObject {
    @Override
    public void Test() {
        LinkedList linkedList = new LinkedList();
        linkedList.addLast(1);
        linkedList.addLast(2);
        linkedList.addLast(4);
        linkedList.addLast(6);
        linkedList.addLast(10);
        linkedList.addLast(11);

        System.out.println(-1*Collections.binarySearch(linkedList, 5));
        System.out.println(Collections.binarySearch(linkedList,12));
        System.out.println(Collections.binarySearch(linkedList,0));
        System.out.println(Collections.binarySearch(linkedList,4));


    }

    @Test
    public void Test2(){

        BPlusTree<Integer> bPlusTree = new BPlusTree(3);
        System.out.println(bPlusTree.insert(7, 2));
        System.out.println(bPlusTree.insert(5, 2));
        System.out.println(bPlusTree.insert(2, 2));
        System.out.println(bPlusTree.insert(4,3));
        System.out.println(bPlusTree.insert(6,3));
        System.out.println(bPlusTree.insert(8,3));
        System.out.println(bPlusTree.insert(1,3));
        System.out.println(bPlusTree.insert(3,3));
        System.out.println(bPlusTree.searchByKey(3));
    }


    @Test
    public void testDIYbinaryTest(){
        ArrayList<Student> students = new ArrayList<>();
        Student student = new Student(10);
        Student student1 = new Student(12);
        Student student2 = new Student(13);
        Student student3 = new Student(13);
        students.add(student);
        students.add(student1);
        students.add(student2);
        int i = Collections.binarySearch(students, student3, (st1, st2) -> {
            if (st1.age == st2.age) {
                return 0;
            }
            return st1.age - st2.age;
        });
        System.out.println(i);
    }



    public static void main(String[] args) {
        TestMasterLog.logTime(()->{
            BitSet bitSet = new BitSet();
            for (int i = 0; i < Integer.MAX_VALUE-1; i++) {
                bitSet.set(i);
            }
            System.out.println(bitSet.nextClearBit(0));
        });
    }

    @Test
    public void testBitSet(){

    }

    class Student{
        int age;

        public Student(int age){
            this.age = age;
        }
    }
}
