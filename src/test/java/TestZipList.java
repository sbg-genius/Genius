import com.genius.Genius.Polymer.Array.ZipList;
import com.genius.Genius.Util.TestMaster.TestMasterLog;
import com.genius.Genius.Util.TestMaster.TestObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Vector;

public class TestZipList extends TestObject {
    @Override
    public void Test() {

    }

    private static final int loop = 10000;
    @Test
    public void TestZipList() {
        ZipList zipList = new ZipList();
        for (int i = 0; i <loop ; i++) {
            zipList.push("hello"+i);
            zipList.push("how are you"+i);
        }
        TestMasterLog.logTime(()->{
            System.out.println(zipList.get(100));
        });

        //System.out.println(RamUsageEstimator.humanSizeOf(zipList));
    }

    @Test
    public void TestVector(){
        Vector<String> vector = new Vector<>();
        for (int i = 0; i <loop ; i++) {
            vector.add("hello"+i);
            vector.add("how are you"+i);
        }
        TestMasterLog.logTime(()->{
            System.out.println(vector.get(100));
        });

       // System.out.println(RamUsageEstimator.humanSizeOf(vector));
    }

    @Test
    public void TestArray(){
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i <loop ; i++) {
            arrayList.add("hello"+i);
            arrayList.add("how are you"+i);
        }
        TestMasterLog.logTime(()->{
            System.out.println(arrayList.get(100));
        });
        //System.out.println(RamUsageEstimator.humanSizeOf(arrayList));
    }
}
