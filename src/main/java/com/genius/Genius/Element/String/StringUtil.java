package com.genius.Genius.Element.String;
import com.genius.Genius.Util.Parser.AnnotationParser;



import java.io.IOException;
import java.io.StringReader;

public class StringUtil extends AnnotationParser {

    public static char[] stringToChars(String temp,char[] chars){
        System.out.println(123);
        for (int i = 0; i <temp.length() ; i++) {
            chars[i] = temp.charAt(i);
        }
        return chars;
    }

    public static void stringToCharsByStream(String temp,char[] chars){
        StringReader sr = new StringReader(temp);
        int index=0;
        try(sr){
            int ch;
            while ((ch=sr.read())!=-1){
                chars[index++]=(char)ch;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
