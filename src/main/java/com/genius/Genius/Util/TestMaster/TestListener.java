package com.genius.Genius.Util.TestMaster;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

public class TestListener extends RunListener {
    private long startTime;
    private long endTime;
    private long methodStartTime;
    private long methodEndTime;
    private String Line = "===============================     %s     ======================================";
    private String DottedLine = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -";

    public void logLine(boolean isLine, String method){
        String line = isLine?String.format(Line,method):DottedLine;
        System.out.println(line);
    }
    @Override
    public void testRunStarted(Description description) throws Exception
    {
        System.out.println("Test Start , Run Methods:" + description.testCount());
        logLine(false,null);
        startTime = System.currentTimeMillis();
    }

    @Override
    public void testStarted(Description description) throws Exception
    {
        logLine(true,description.getMethodName() + " Method Started!");
        methodStartTime = System.currentTimeMillis();
    }

    @Override
    public void testFinished(Description description) throws Exception
    {
        methodEndTime = System.currentTimeMillis();
        System.out.println(String.format(TestMasterLog.LOG_TIME,(methodEndTime - methodStartTime) ));
        logLine(true,description.getMethodName() + " Method Ended!");
    }

    @Override
    public void testRunFinished(Result result) throws Exception
    {
        endTime = System.currentTimeMillis();
        logLine(false,null);
        System.out.println("All Test Run Finished!");
        System.out.println("Run times:" + result.getRunCount()+" All Time(ms)" + (endTime - startTime) );
    }

    @Override
    public void testFailure(Failure failure) throws Exception
    {
        System.out.println("Method Named " + failure.getDescription().getMethodName() + " Failed!");
        System.out.println("  Exception : " + failure.getException());
    }


}
