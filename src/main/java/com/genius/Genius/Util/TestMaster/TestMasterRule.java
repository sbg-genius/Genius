package com.genius.Genius.Util.TestMaster;


import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class TestMasterRule implements TestRule {

    private TestFunction testFunction;
    private loopTestFunction loopFunction;

    public TestMasterRule(TestFunction testFunction){
        this.testFunction = testFunction;
    }

    public TestMasterRule(loopTestFunction loopFunction) {
        this.loopFunction = loopFunction;
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                if(testFunction!=null){
                    base.evaluate();
                    testFunction.test();
                }else{
                    loopFunction.test(()->{
                        try {
                            base.evaluate();
                        }catch (Throwable e){
                            e.printStackTrace();
                        }
                    });
                }
            }
        };
    }
}
