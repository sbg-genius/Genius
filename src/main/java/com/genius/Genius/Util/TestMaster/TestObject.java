package com.genius.Genius.Util.TestMaster;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(TestRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public abstract class TestObject  {
    @Test
    public abstract void Test();
}
