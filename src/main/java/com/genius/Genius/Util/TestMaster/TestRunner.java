package com.genius.Genius.Util.TestMaster;

import org.junit.internal.AssumptionViolatedException;
import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.runner.notification.RunNotifier;
import org.junit.runner.notification.StoppedByUserException;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

public class TestRunner extends BlockJUnit4ClassRunner {

    public TestRunner(Class<?> Class) throws InitializationError {
        super(Class);
    }

    @Override
    public void run(RunNotifier notifier)
    {
        notifier.addListener(new TestListener());
        EachTestNotifier testNotifier = new EachTestNotifier(notifier, getDescription());
        notifier.fireTestRunStarted(getDescription());
        try
        {
            Statement statement = classBlock(notifier);
            statement.evaluate();
        }
        catch(AssumptionViolatedException av)
        {
            testNotifier.addFailedAssumption(av);
        }
        catch(StoppedByUserException sbue)
        {
            throw sbue;
        }
        catch(Throwable e)
        {
            testNotifier.addFailure(e);
        }
    }
}
