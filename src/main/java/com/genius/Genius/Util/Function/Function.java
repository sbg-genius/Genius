package com.genius.Genius.Util.Function;

@FunctionalInterface
public interface Function<R,T,F> {
    public R apply(T t,F n);
}
