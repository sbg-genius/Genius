package com.genius.Genius.Util.Unsafe;

import sun.misc.Unsafe;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class JavaUnSafe {
    public static Unsafe getUnsafe(){
        Class<Unsafe> unsafeClass = Unsafe.class;
        Unsafe unsafe = null;
        try {
            Constructor<Unsafe> constructor = unsafeClass.getDeclaredConstructor();
            constructor.setAccessible(true);
            unsafe = constructor.newInstance();
        } catch (NoSuchMethodException |InstantiationException | IllegalAccessException |InvocationTargetException e) {
            e.printStackTrace();
        }
       return unsafe;
    }
}
